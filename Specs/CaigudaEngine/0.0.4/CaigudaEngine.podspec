Pod::Spec.new do |s|
  
  #root
	s.name     = 'CaigudaEngine'
	s.version  = '0.0.4'
	s.license  = 'Apache License, Version 2.0'
	s.homepage = 'http://caiguda.com'
	s.author   = 'Malaar'
  s.summary = ' 2D Game engine. Contains next engines: Graphic, Script, Sound, Particles.'
	s.source   = { :git => 'https://Malaar@bitbucket.org/Malaar/caigudaengine.git', :tag => '0.0.4' }

  #platform  
	s.ios.deployment_target = '5.0'

  #build settings
	s.frameworks   = 'GLKit', 'CoreGraphics', 'OpenAL', 'OpenGLES', 'QuartzCore', 'CoreMotion'
	s.requires_arc = true
  s.xcconfig = { "OTHER_LDFLAGS" => '-lObjC', "GCC_PREPROCESSOR_DEFINITIONS" => 'X_DEBUG X_SHOW_FPS X_OS_IOS X_STATIC_LIB'}
  s.prefix_header_file = 'Projects/CaigudaEngineiOS/CaigudaEngine/CaigudaEngine-Prefix.pch'
  
  #file patterns
	s.source_files = 'Sources/**/*.{h,m,mm,c,cpp,lua}'
	s.exclude_files = 'Sources/**/{QT,3D}/**/*'
  s.resources = '**/*.lua'

end
